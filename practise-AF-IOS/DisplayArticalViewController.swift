//
//  DisplayArticalViewController.swift
//  practise-AF-IOS
//
//  Created by KSGA-004 on 12/7/20.
//

import UIKit
import Alamofire
import SwiftyJSON
protocol EditArtical {
    func EditDeletegate( title: String, des: String)
}
protocol AddArticle {
    func addDeletegate( title: String, des: String)
}
class DisplayArticalViewController: UIViewController, SentArtical {

    @IBOutlet weak var des: UITextView!
    @IBOutlet weak var titleArtical: UITextView!
    var titleArt: String = ""
    var desArt: String = ""
    var id: String = ""
    var editDelegate: EditArtical?
    var addDelegate: AddArticle?
    override func viewDidLoad() {
        super.viewDidLoad()

        titleArtical.text = titleArt
        des.text = desArt
    }
    override func viewWillDisappear(_ animated: Bool) {
        editDelegate?.EditDeletegate(title: titleArtical.text, des: des.text)
        addDelegate?.addDeletegate(title: titleArtical.text, des: des.text)
    }
    func sentData(id: String,title: String, des: String) {
        self.titleArt = title
        self.desArt = des
        self.id = id
    }
    func deleteArtical(id: String) {
        AF.request("http://110.74.194.124:3000/api/articles/\(id)", method: .delete).responseJSON(completionHandler: { response in
            switch response.result {
            case .success(let value):
               print(value)
            case .failure(let erorr):
                print(erorr)
            }
           
        })
    }
    @IBAction func deletePress(_ sender: Any) {
        let alert = UIAlertController(title: "", message: "Do you want to delete this article?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { _ in
            let alert = UIAlertController(title: "", message: "Deleted successfully.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Done", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            self.deleteArtical(id: self.id)
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil
        ))
        present(alert, animated: true, completion: nil)
       
    }
}
