//
//  ViewController.swift
//  practise-AF-IOS
//
//  Created by KSGA-004 on 12/7/20.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol SentArtical {
    func sentData(id: String,title: String, des: String)
}
class ViewController: UIViewController, EditArtical, AddArticle {

    @IBOutlet weak var table: UITableView!
    
    @IBOutlet weak var addArticle: UIBarButtonItem!
    
    var sentDelegate: SentArtical?
    var modelArtical: [Artical] = []
    var artical = Artical()
    var indexPath: Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchaData()
    }
    
    func fetchaData() {
        AF.request("http://110.74.194.124:3000/api/articles").response { reponse in
            switch reponse.result{
            case .success(let value):
                let data = JSON(value!)
                var arrArticle = [Artical]()
                for (_, subJson):(String, JSON) in data["data"] {
                    self.artical = Artical(json: subJson)
                    arrArticle.append(self.artical)
                }
                self.modelArtical = arrArticle
                    self.table.reloadData()
            case .failure(let erorr):
                print(":",erorr)
            }
        }
    }
    func deleteArtical(id: String) {
        AF.request("http://110.74.194.124:3000/api/articles/\(id)", method: .delete).responseJSON(completionHandler: { response in
            switch response.result {
            case .success(let value):
                let data = JSON(value)
                self.Success(message: data["message"].stringValue)
                self.fetchaData()
            case .failure(let erorr):
                print(erorr)
            }
           
        })
    }
    func editArticle(title: String, des: String) {
        let data: Parameters = [
            "title": title,
            "description": des,
            "published": "true",
            "image": "string"
          ]
        AF.request("http://110.74.194.124:3000/api/articles/\(modelArtical[indexPath!].id)", method: .patch, parameters: data, encoding: JSONEncoding.default).responseJSON(completionHandler: { response in
            switch response.result{
            case .success(let value):
                print("=>sucess",value)
                self.fetchaData()
            case .failure(let error):
                print("=>error",error)
            }
            
        })
    }
    func addArticle(title: String, des: String) {
        let param: Parameters = [
            "title": title,
             "description": des,
             "published": true,
             "image": "string"
        ]
        AF.request("http://110.74.194.124:3000/api/articles", method: .post, parameters: param, encoding: JSONEncoding.default).responseJSON(completionHandler: { response in
            switch response.result{
            case .success(let value):
                print(value)
                self.fetchaData()
            case .failure(let error):
                print(error)
            }
             
        })
    }
    func addDeletegate(title: String, des: String) {
        addArticle(title: title, des: des)
    }
    
    @IBAction func addPress(_ sender: Any) {
        let secondVC = storyboard?.instantiateViewController(identifier: "displayVC") as! DisplayArticalViewController
        secondVC.addDelegate = self
        self.navigationController?.pushViewController(secondVC, animated: true)
    }
    func EditDeletegate(title: String, des: String) {
        editArticle(title: title, des: des)
    }
    func Success(message: String) {
        let alert = UIAlertController(title: "", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: {_ in 
        
        }))
       
        self.present(alert, animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, contextMenuConfigurationForRowAt indexPath: IndexPath, point: CGPoint) -> UIContextMenuConfiguration? {
        let shareAction = UIAction(title: "Share", image: UIImage(systemName: "square.and.arrow.up")) { _ in
        print("Share")
        }
        let copy = UIAction(title: "Copy", image: UIImage(systemName: "doc.on.doc")) { _ in
        print("Copy")
        }
        let delete = UIAction(title: "Delete", image: UIImage(systemName: "delete.right")) { _ in
            
            let alert = UIAlertController(title: "", message: "Do you want to delete this artical", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { _ in
                self.deleteArtical(id: self.modelArtical[indexPath.row].id)
                self.fetchaData()
                
            }))
            alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
            
        }
        // Pass the indexPath as the identifier for the menu configuration
        return UIContextMenuConfiguration(identifier: indexPath as NSIndexPath, previewProvider: nil) { _ in
            return UIMenu(title: "", children: [shareAction, copy, delete])
        }
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return modelArtical.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = self.modelArtical[indexPath.row].title
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.indexPath = indexPath.row
        let secondVC = storyboard?.instantiateViewController(identifier: "displayVC") as! DisplayArticalViewController
        self.sentDelegate = secondVC
        secondVC.editDelegate = self
        sentDelegate?.sentData(id: self.modelArtical[indexPath.row].id, title: self.modelArtical[indexPath.row].title, des: self.modelArtical[indexPath.row].description)
        self.navigationController?.pushViewController(secondVC, animated: true)
    }
}
