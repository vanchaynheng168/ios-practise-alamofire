//
//  Artical.swift
//  practise-AF-IOS
//
//  Created by KSGA-004 on 12/7/20.
//

import Foundation
import SwiftyJSON

struct Artical {
    var id: String = ""
    var description: String = ""
    var title: String = ""
    init() {}
    init(json: JSON) {
        id = json["_id"].stringValue
        description = json["description"].stringValue
        title = json["title"].stringValue
    }
    
}
